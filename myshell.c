
#include "myshell.h"	 

//function prototypes below
void runCommand();
void error(char msg[MAX_BUFFER]);
void updatePath();
void tokenize();
void sigChild();
void ioRedirect(int readAllowed);
void ioRedirectClose();
void readBatchFile(char filename[MAX_FILENAME]);


//setting up variables
extern char **environ;
char input[MAX_BUFFER];	        
char *args[MAX_ARGS];		        
char **arg;			        
int argn;			        
char initPath[MAX_BUFFER];	       
char tempPath[MAX_BUFFER];		        
char currPath[MAX_BUFFER];	        

char redfile[3][MAX_FILENAME]; 	        
char redirectFlag[3];

//create list of essential strings for comparison later
char *redchars[3] = { ">>", ">",  "<" };   

int main (int argc, char * argv[]) {
  //check if the system is functional with the signint, the second signal is not necessary
  //therefore we use an ignore.
  signal(SIGINT, SIG_IGN); 	        
	
  //finding the initial path of the c shell script run.
  getcwd(initPath, MAX_BUFFER-1);       
  //copying the initial path into current path to keep track of.
  strcpy(currPath, initPath);
  //adding /myshell such that it prints on each step, as required in the specification.
  strcat(currPath, "/myshell");
  //setting the environment path to SHELL
  setenv("SHELL", currPath, 1);	        
	
  //checking argument length is valid, as well for batchfile running.
  if(argc > 2) {
    //send error message.
    error("Max arg list length = 2");
  } else if(argc == 2) {
    //go to batch file reading, with the second argument from argument list.
    readBatchFile(argv[1]);	        
  }

  //go through the user input.
  while (!feof(stdin)) {
    
    
    updatePath();
    //printing the current path for the user.
    printf("%s$ ",tempPath);		        
		
    //gets the current line that has been inputted
    if (fgets (input, MAX_BUFFER, stdin )) { 
      //
      tokenize(); 			        
      int background = 0;
      
      
      if(argn > 1 && !strcmp(args[argn - 1], "&")) {
        
	    args[argn-1] = (char*)0;		        
	    background = 1;
      }
			
      if(background) {
        //need to run the shell as well as the program that has been given to the shell
	    switch(fork()) {
        case -1:
          //couldnt fork 
	      error("fork error");
	      break;
	    case 0:
          //change the environment to parent if child was successfully created
	      setenv("PARENT", currPath, 1);
	      runCommand();
          //once sub function has completed, need to quit.
	      exit(0);		        
	      break;
	    }
      } else {
	    runCommand();
      }
    }
  }
  return 0; 
}

void error(char msg[MAX_BUFFER]) {
  printf("%s.\n", msg);
  exit(1);
}

void updatePath() {
  //puts the current pathname of the system in tempPath
  getcwd(tempPath, MAX_BUFFER-1);
}

void tokenize() {
  char *tmp, *point;
  int i;
  
  //going through input lines and placing into tokens
  for(i = 0; i < 3; i++) {
    //setting initial redirectFlag of current space 0
    redirectFlag[i] = 0;
    //checking for redirect chars inside of the input line as well as setting point and then checking it
    if((point = strstr(input,redchars[i])) != NULL) {  
      //the next word of the line is taken, after separators and the current redirect characters are taken into account
      tmp = strtok(point+strlen(redchars[i]), SEPARATORS);       
      //copy the taken word into redirect files [i]
      strcpy(redfile[i],tmp);
      //change redirect flag to 1, as we need to know whether to use later.
      redirectFlag[i] = 1;				        

      //reset point to end line character
      *point = '\0';
      //get the next word into tmp, add that word to the string - input
      if((tmp=strtok(NULL,SEPARATORS)) != NULL) strcat(input,tmp);			        
    }
  }

  //at least 1 argument must have been inputted to get here, therefore we start argn at 1
  argn = 1;
  //temp store for current args
  arg = args;
  //start going through arg to get total number of args in argn 
  *arg++ = strtok(input,SEPARATORS);
  //go through input, adding each word to number of args.
  while ((*arg++ = strtok(NULL,SEPARATORS)))      
    argn++;
}

void sigChild() {
  //check that the child is functional, responding
  signal(SIGINT, SIG_IGN);
  //move to the next line
  putchar('\n');
}

void ioRedirect(int readAllowed) {
  //depending on flag that were found when tokenizing, either input or output to files, (i/o redirection as in spec)
  if(redirectFlag[0] == 1) freopen(redfile[0], "a", stdout);        
  if(redirectFlag[1] == 1) freopen(redfile[1], "w", stdout);	  
  //make sure permissions have been granted, if so open specified file for reading
  if(redirectFlag[2] == 1 && readAllowed == 1) freopen(redfile[2], "r", stdin);   
}

void ioRedirectClose() {
  //go to previous session of the shell, stored in dev/tty
  if(redirectFlag[0] || redirectFlag[1]) freopen("/dev/tty","w",stdout);		        	
}


void runCommand() {
  //checking if there is a command that exists
  if (args[0]) {	 
    //clear screen if arg0 is clear
    if (!strcmp(args[0],"clr")) {
      system("clear");
      
    } else if(!strcmp(args[0],"dir")) {
      //get necessary file destination for dir
      ioRedirect(0);
      char tmp[MAX_BUFFER];
      //put command in tmp
      strcpy(tmp, "ls -la ");
      // add args1 to tmp , as well as permissions, ownership ect..
      if(args[1]) strcat(tmp, args[1]);
      //get the system to print out created command
      system(tmp);		         
      //get back to previous shell stored
      ioRedirectClose();	        
    }

    //list all environment variables
    else if(!strcmp(args[0],"environ")) {
      //get necessary file destination for environ
      ioRedirect(0);
      //get current environment in temp env
      char **env = environ;
      //go through env and print them
      while( *env ) {
	    printf( "%s\n", *env );
	    env++;
      }
      //go back to prev state
      ioRedirectClose();	        
    }

    else if(!strcmp(args[0], "cd")) {
      //if no other args just print the path
      if(!args[1]) {		        
        printf( "%s\n", tempPath);	        
      } else {
        //go to the specified directory 
	    if(!chdir(args[1])) {   
	      updatePath();
	      setenv("PWD", tempPath, 1);		
	    } else {	   
          //appropriate error message 
	      printf("%s: No such file or directory exist\n", args[1]);
	    }
      }
    }
		
    else if (!strcmp(args[0], "echo")) {
      //if words exist after the echo, deal with them
      ioRedirect(0);			       
			
      char *comment = (char *)malloc(MAX_BUFFER);
      strcpy(comment, "");
      //copy memory location of args1 into arg, as to properly concatencate and later print
      arg = &args[1];
      
      //add each argument as well as a space
      while (*arg) {
        strcat(comment, *argn++);
	    strcat(comment, " ");        
      }
		
      //display echoed comment
      printf("%s\n", comment);
      //reset comments memory space, then free
      memset(comment, 0, MAX_BUFFER); 
      free(comment);
      
      //close any files that were opened due to i/o redirection
      ioRedirectClose();			
    } else if (!strcmp(args[0], "help")) {
      ioRedirect(1);
      //need access to open the readme file for to display help
			
      char tmp[MAX_BUFFER];
      //get the full path of the readme as well as the more operation, to view the text from the readme
      strcpy(tmp, "more ");
      strcat(tmp, initPath);
      strcat(tmp, "/readme");
      system(tmp);
      putchar('\n');
		
      //close the readme file
      ioRedirectClose();
    } else if(!strcmp(args[0], "pause")) {
      //set a password to continue with getpass
      getpass("Press Enter to continue\n");   
    } else if (!strcmp(args[0],"quit")) {
      //quit program
      exit(0);
    } else {	
      //running a program, as no keyword specified
      int status;
      pid_t pid;
      signal(SIGINT, sigChild); 	        

      //run the program as a fork of the current program
      switch(pid = fork()) {
      case -1:
	    error("fork error");
	    break;
      case 0: 		        
	    setenv("PARENT", currPath, 1);
	    ioRedirect(1);			        
					
      //executing the file, along with given arguments, if not successful printing and exit with error code
	  if (execvp(args[0],args) == -1) {
        error("command not found");		
	    exit(1); 			        
	    break;
      }
    }
    //clear output buffer
    fflush(stdout);
    //if behind another system, wait to catch up
    waitpid(pid, &status, 0);	        
  }
}

void readBatchFile(char filename[MAX_FILENAME]) {
  FILE *fp;
  int lineNo = 1;

  //attempt tp open given file, if unsuccessful return error.
  fp = fopen(filename, "r");	        
  if(fp == NULL) error("Batch file does not exists");
	
  //read from file, line at a time, as to execute each one, just as user input
  while (fgets (input, MAX_BUFFER, fp )) {
    printf("%d. %s", lineNo++, input);  
    //convert into to tokens
    tokenize();
    //run each command in the batchfile
    runCommand();
    //print a newline for each command completed
    putchar('\n');
  }
  //close the file once read and executed
  fclose(fp);
  //quit the shell
  exit(0);			        
}


